/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import BorrowDetailComponent from '@/entities/borrow/borrow-details.vue';
import BorrowClass from '@/entities/borrow/borrow-details.component';
import BorrowService from '@/entities/borrow/borrow.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Borrow Management Detail Component', () => {
    let wrapper: Wrapper<BorrowClass>;
    let comp: BorrowClass;
    let borrowServiceStub: SinonStubbedInstance<BorrowService>;

    beforeEach(() => {
      borrowServiceStub = sinon.createStubInstance<BorrowService>(BorrowService);

      wrapper = shallowMount<BorrowClass>(BorrowDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { borrowService: () => borrowServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundBorrow = { id: 123 };
        borrowServiceStub.find.resolves(foundBorrow);

        // WHEN
        comp.retrieveBorrow(123);
        await comp.$nextTick();

        // THEN
        expect(comp.borrow).toBe(foundBorrow);
      });
    });
  });
});
