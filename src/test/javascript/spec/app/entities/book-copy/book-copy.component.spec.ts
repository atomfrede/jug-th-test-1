/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import BookCopyComponent from '@/entities/book-copy/book-copy.vue';
import BookCopyClass from '@/entities/book-copy/book-copy.component';
import BookCopyService from '@/entities/book-copy/book-copy.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {}
  }
};

describe('Component Tests', () => {
  describe('BookCopy Management Component', () => {
    let wrapper: Wrapper<BookCopyClass>;
    let comp: BookCopyClass;
    let bookCopyServiceStub: SinonStubbedInstance<BookCopyService>;

    beforeEach(() => {
      bookCopyServiceStub = sinon.createStubInstance<BookCopyService>(BookCopyService);
      bookCopyServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<BookCopyClass>(BookCopyComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          bookCopyService: () => bookCopyServiceStub
        }
      });
      comp = wrapper.vm;
    });

    it('should be a Vue instance', () => {
      expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('Should call load all on init', async () => {
      // GIVEN
      bookCopyServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllBookCopys();
      await comp.$nextTick();

      // THEN
      expect(bookCopyServiceStub.retrieve.called).toBeTruthy();
      expect(comp.bookCopies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      bookCopyServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeBookCopy();
      await comp.$nextTick();

      // THEN
      expect(bookCopyServiceStub.delete.called).toBeTruthy();
      expect(bookCopyServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
