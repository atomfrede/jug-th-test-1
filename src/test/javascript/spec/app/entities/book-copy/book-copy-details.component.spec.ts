/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import BookCopyDetailComponent from '@/entities/book-copy/book-copy-details.vue';
import BookCopyClass from '@/entities/book-copy/book-copy-details.component';
import BookCopyService from '@/entities/book-copy/book-copy.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('BookCopy Management Detail Component', () => {
    let wrapper: Wrapper<BookCopyClass>;
    let comp: BookCopyClass;
    let bookCopyServiceStub: SinonStubbedInstance<BookCopyService>;

    beforeEach(() => {
      bookCopyServiceStub = sinon.createStubInstance<BookCopyService>(BookCopyService);

      wrapper = shallowMount<BookCopyClass>(BookCopyDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { bookCopyService: () => bookCopyServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundBookCopy = { id: 123 };
        bookCopyServiceStub.find.resolves(foundBookCopy);

        // WHEN
        comp.retrieveBookCopy(123);
        await comp.$nextTick();

        // THEN
        expect(comp.bookCopy).toBe(foundBookCopy);
      });
    });
  });
});
