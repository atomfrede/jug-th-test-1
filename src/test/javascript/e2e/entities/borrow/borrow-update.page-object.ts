import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class BorrowUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('jhthApp.borrow.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  borrowedAtInput: ElementFinder = element(by.css('input#borrow-borrowedAt'));

  bookcopySelect = element(by.css('select#borrow-bookcopy'));

  userSelect = element(by.css('select#borrow-user'));
}
