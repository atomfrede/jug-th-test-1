package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Borrow;
import com.mycompany.myapp.repository.BorrowRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Borrow}.
 */
@RestController
@RequestMapping("/api")
public class BorrowResource {

    private final Logger log = LoggerFactory.getLogger(BorrowResource.class);

    private static final String ENTITY_NAME = "borrow";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BorrowRepository borrowRepository;

    public BorrowResource(BorrowRepository borrowRepository) {
        this.borrowRepository = borrowRepository;
    }

    /**
     * {@code POST  /borrows} : Create a new borrow.
     *
     * @param borrow the borrow to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new borrow, or with status {@code 400 (Bad Request)} if the borrow has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/borrows")
    public ResponseEntity<Borrow> createBorrow(@Valid @RequestBody Borrow borrow) throws URISyntaxException {
        log.debug("REST request to save Borrow : {}", borrow);
        if (borrow.getId() != null) {
            throw new BadRequestAlertException("A new borrow cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Borrow result = borrowRepository.save(borrow);
        return ResponseEntity.created(new URI("/api/borrows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /borrows} : Updates an existing borrow.
     *
     * @param borrow the borrow to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated borrow,
     * or with status {@code 400 (Bad Request)} if the borrow is not valid,
     * or with status {@code 500 (Internal Server Error)} if the borrow couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/borrows")
    public ResponseEntity<Borrow> updateBorrow(@Valid @RequestBody Borrow borrow) throws URISyntaxException {
        log.debug("REST request to update Borrow : {}", borrow);
        if (borrow.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Borrow result = borrowRepository.save(borrow);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, borrow.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /borrows} : get all the borrows.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of borrows in body.
     */
    @GetMapping("/borrows")
    public List<Borrow> getAllBorrows() {
        log.debug("REST request to get all Borrows");
        return borrowRepository.findAll();
    }

    /**
     * {@code GET  /borrows/:id} : get the "id" borrow.
     *
     * @param id the id of the borrow to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the borrow, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/borrows/{id}")
    public ResponseEntity<Borrow> getBorrow(@PathVariable Long id) {
        log.debug("REST request to get Borrow : {}", id);
        Optional<Borrow> borrow = borrowRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(borrow);
    }

    /**
     * {@code DELETE  /borrows/:id} : delete the "id" borrow.
     *
     * @param id the id of the borrow to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/borrows/{id}")
    public ResponseEntity<Void> deleteBorrow(@PathVariable Long id) {
        log.debug("REST request to delete Borrow : {}", id);
        borrowRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
