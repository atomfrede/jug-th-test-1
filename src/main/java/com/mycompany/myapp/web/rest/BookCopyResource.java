package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.BookCopy;
import com.mycompany.myapp.repository.BookCopyRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.BookCopy}.
 */
@RestController
@RequestMapping("/api")
public class BookCopyResource {

    private final Logger log = LoggerFactory.getLogger(BookCopyResource.class);

    private static final String ENTITY_NAME = "bookCopy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookCopyRepository bookCopyRepository;

    public BookCopyResource(BookCopyRepository bookCopyRepository) {
        this.bookCopyRepository = bookCopyRepository;
    }

    /**
     * {@code POST  /book-copies} : Create a new bookCopy.
     *
     * @param bookCopy the bookCopy to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bookCopy, or with status {@code 400 (Bad Request)} if the bookCopy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/book-copies")
    public ResponseEntity<BookCopy> createBookCopy(@Valid @RequestBody BookCopy bookCopy) throws URISyntaxException {
        log.debug("REST request to save BookCopy : {}", bookCopy);
        if (bookCopy.getId() != null) {
            throw new BadRequestAlertException("A new bookCopy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookCopy result = bookCopyRepository.save(bookCopy);
        return ResponseEntity.created(new URI("/api/book-copies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /book-copies} : Updates an existing bookCopy.
     *
     * @param bookCopy the bookCopy to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookCopy,
     * or with status {@code 400 (Bad Request)} if the bookCopy is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bookCopy couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/book-copies")
    public ResponseEntity<BookCopy> updateBookCopy(@Valid @RequestBody BookCopy bookCopy) throws URISyntaxException {
        log.debug("REST request to update BookCopy : {}", bookCopy);
        if (bookCopy.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BookCopy result = bookCopyRepository.save(bookCopy);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bookCopy.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /book-copies} : get all the bookCopies.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookCopies in body.
     */
    @GetMapping("/book-copies")
    public List<BookCopy> getAllBookCopies() {
        log.debug("REST request to get all BookCopies");
        return bookCopyRepository.findAll();
    }

    /**
     * {@code GET  /book-copies/:id} : get the "id" bookCopy.
     *
     * @param id the id of the bookCopy to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bookCopy, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/book-copies/{id}")
    public ResponseEntity<BookCopy> getBookCopy(@PathVariable Long id) {
        log.debug("REST request to get BookCopy : {}", id);
        Optional<BookCopy> bookCopy = bookCopyRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bookCopy);
    }

    /**
     * {@code DELETE  /book-copies/:id} : delete the "id" bookCopy.
     *
     * @param id the id of the bookCopy to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/book-copies/{id}")
    public ResponseEntity<Void> deleteBookCopy(@PathVariable Long id) {
        log.debug("REST request to delete BookCopy : {}", id);
        bookCopyRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
