package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Borrow.
 */
@Entity
@Table(name = "borrow")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Borrow implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "borrowed_at", nullable = false)
    private LocalDate borrowedAt;

    @ManyToOne
    @JsonIgnoreProperties("borrows")
    private BookCopy bookcopy;

    @ManyToOne
    @JsonIgnoreProperties("borrows")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getBorrowedAt() {
        return borrowedAt;
    }

    public Borrow borrowedAt(LocalDate borrowedAt) {
        this.borrowedAt = borrowedAt;
        return this;
    }

    public void setBorrowedAt(LocalDate borrowedAt) {
        this.borrowedAt = borrowedAt;
    }

    public BookCopy getBookcopy() {
        return bookcopy;
    }

    public Borrow bookcopy(BookCopy bookCopy) {
        this.bookcopy = bookCopy;
        return this;
    }

    public void setBookcopy(BookCopy bookCopy) {
        this.bookcopy = bookCopy;
    }

    public User getUser() {
        return user;
    }

    public Borrow user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Borrow)) {
            return false;
        }
        return id != null && id.equals(((Borrow) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Borrow{" +
            "id=" + getId() +
            ", borrowedAt='" + getBorrowedAt() + "'" +
            "}";
    }
}
