import { mixins } from 'vue-class-component';

import { Component, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IBookCopy } from '@/shared/model/book-copy.model';
import AlertService from '@/shared/alert/alert.service';

import BookCopyService from './book-copy.service';

@Component
export default class BookCopy extends mixins(Vue2Filters.mixin) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('bookCopyService') private bookCopyService: () => BookCopyService;
  private removeId: number = null;
  public bookCopies: IBookCopy[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllBookCopys();
  }

  public clear(): void {
    this.retrieveAllBookCopys();
  }

  public retrieveAllBookCopys(): void {
    this.isFetching = true;

    this.bookCopyService()
      .retrieve()
      .then(
        res => {
          this.bookCopies = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IBookCopy): void {
    this.removeId = instance.id;
  }

  public removeBookCopy(): void {
    this.bookCopyService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('jhthApp.bookCopy.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllBookCopys();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
