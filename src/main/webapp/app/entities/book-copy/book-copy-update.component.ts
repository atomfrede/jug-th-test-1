import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IBookCopy, BookCopy } from '@/shared/model/book-copy.model';
import BookCopyService from './book-copy.service';

const validations: any = {
  bookCopy: {
    signature: {
      required
    },
    title: {
      required
    }
  }
};

@Component({
  validations
})
export default class BookCopyUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('bookCopyService') private bookCopyService: () => BookCopyService;
  public bookCopy: IBookCopy = new BookCopy();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.bookCopyId) {
        vm.retrieveBookCopy(to.params.bookCopyId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.bookCopy.id) {
      this.bookCopyService()
        .update(this.bookCopy)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('jhthApp.bookCopy.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.bookCopyService()
        .create(this.bookCopy)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('jhthApp.bookCopy.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveBookCopy(bookCopyId): void {
    this.bookCopyService()
      .find(bookCopyId)
      .then(res => {
        this.bookCopy = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
