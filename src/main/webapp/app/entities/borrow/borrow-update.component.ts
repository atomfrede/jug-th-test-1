import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import BookCopyService from '../book-copy/book-copy.service';
import { IBookCopy } from '@/shared/model/book-copy.model';

import UserService from '@/admin/user-management/user-management.service';

import AlertService from '@/shared/alert/alert.service';
import { IBorrow, Borrow } from '@/shared/model/borrow.model';
import BorrowService from './borrow.service';

const validations: any = {
  borrow: {
    borrowedAt: {
      required
    }
  }
};

@Component({
  validations
})
export default class BorrowUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('borrowService') private borrowService: () => BorrowService;
  public borrow: IBorrow = new Borrow();

  @Inject('bookCopyService') private bookCopyService: () => BookCopyService;

  public bookCopies: IBookCopy[] = [];

  @Inject('userService') private userService: () => UserService;

  public users: Array<any> = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.borrowId) {
        vm.retrieveBorrow(to.params.borrowId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.borrow.id) {
      this.borrowService()
        .update(this.borrow)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('jhthApp.borrow.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.borrowService()
        .create(this.borrow)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('jhthApp.borrow.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveBorrow(borrowId): void {
    this.borrowService()
      .find(borrowId)
      .then(res => {
        this.borrow = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.bookCopyService()
      .retrieve()
      .then(res => {
        this.bookCopies = res.data;
      });
    this.userService()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
