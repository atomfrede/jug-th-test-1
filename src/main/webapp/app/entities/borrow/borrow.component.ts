import { mixins } from 'vue-class-component';

import { Component, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IBorrow } from '@/shared/model/borrow.model';
import AlertService from '@/shared/alert/alert.service';

import BorrowService from './borrow.service';

@Component
export default class Borrow extends mixins(Vue2Filters.mixin) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('borrowService') private borrowService: () => BorrowService;
  private removeId: number = null;
  public borrows: IBorrow[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllBorrows();
  }

  public clear(): void {
    this.retrieveAllBorrows();
  }

  public retrieveAllBorrows(): void {
    this.isFetching = true;

    this.borrowService()
      .retrieve()
      .then(
        res => {
          this.borrows = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IBorrow): void {
    this.removeId = instance.id;
  }

  public removeBorrow(): void {
    this.borrowService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('jhthApp.borrow.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllBorrows();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
