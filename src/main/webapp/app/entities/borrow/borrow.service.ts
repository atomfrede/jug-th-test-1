import axios from 'axios';

import { IBorrow } from '@/shared/model/borrow.model';

const baseApiUrl = 'api/borrows';

export default class BorrowService {
  public find(id: number): Promise<IBorrow> {
    return new Promise<IBorrow>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IBorrow): Promise<IBorrow> {
    return new Promise<IBorrow>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IBorrow): Promise<IBorrow> {
    return new Promise<IBorrow>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
